# Kiuwan.sh runs the Kiuwan agent in docker and passes all args to the agent in the docker
#!/bin/sh
set -ex
docker run shiftleftpt/kiuwan:latest /KiuwanLocalAnalyzer/bin/agent.sh "$@"
