# bumps the build number
#!/bin/sh
BASEDIR=$(dirname "$0")
perl -i -pe 's /\d+\.\d+\.\K(\d+)/ $1+1 /e' $BASEDIR/../VERSION
