# runs docker build with 2 args passed Kiuawn user name and password
#!/bin/sh
BASEDIR=$(dirname "$0")
docker build --pull --no-cache --rm -t shiftleftpt/kiuwan:latest --build-arg "Q1USER=$1" --build-arg "Q1PASS=$2" $BASEDIR/../.

