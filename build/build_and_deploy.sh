# Builds the current VERSION of the dockerfile.
# Runs Builds, Release after tag to Docker Hub and Tags source to GIT
# Takes 2 args...The Kiuwan user name and password needed to run kiuwan once so that the image is nice and ready for reuse!
#!/bin/sh
set -ex
BASEDIR=$(dirname "$0")
"$BASEDIR"/bump.sh
"$BASEDIR"/build_dockerhub.sh "$@"
"$BASEDIR"/release_dockerhub.sh "$@"
"$BASEDIR"/tag_code_git.sh "$@"
