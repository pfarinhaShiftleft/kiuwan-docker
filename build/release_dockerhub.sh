#!/bin/sh
set -ex
IMAGE=kiuwan
USER=shiftleftpt
version=`cat VERSION`
echo "version: $version"
docker tag $USER/$IMAGE:latest $USER/$IMAGE:$version
# push it
docker push $USER/$IMAGE:latest
docker push $USER/$IMAGE:$version