FROM openjdk:8-jre-alpine
ARG Q1USER
ARG Q1PASS

RUN apk add --no-cache bash
RUN apk add --update curl && rm -rf /var/cache/apk/*
RUN curl -O https://www.kiuwan.com/pub/analyzer/KiuwanLocalAnalyzer.zip 
RUN unzip KiuwanLocalAnalyzer.zip 
RUN rm KiuwanLocalAnalyzer.zip 
COPY ./docker-entrypoint.sh /
COPY ./src /src
RUN chmod +x /docker-entrypoint.sh
#RUN addgroup -S -g 1000 jenkins
#RUN adduser -S -D -u 1000 -s /sbin/nologin -h /KiuwanLocalAnalyzer -g "Jenkins User" -G jenkins jenkins
#RUN chown jenkins:jenkins -R  /KiuwanLocalAnalyzer
#USER jenkins

RUN cd $JAVA_HOME/lib/security && sed -i 's/securerandom.source=file:\/dev\/random/securerandom.source=file:\/dev\/urandom/g' java.security
RUN /KiuwanLocalAnalyzer/bin/agent.sh -s /src -n KiuwanDocker -c --user $Q1USER --pass $Q1PASS
ADD VERSION .
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/KiuwanLocalAnalyzer/bin/agent.sh", "--help"]
