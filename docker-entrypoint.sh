#!/bin/sh 
set -e

# this if image does not requires any specific setup.
# defaults to run whatever the user wanted like "bash" or "sh"
exec "$@"